package ru.kav.zadachi.zd10;
import java.util.Random;
import java.util.Scanner;
/**
 * Created by user on 06.04.2017.
 */
public class Palindrom {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        int input;
        System.out.println("Введите ваше число");
        input = scanner.nextInt();
        System.out.println(isPalindrom(input));

    }
    static int InvertNumber(int input) {
        int i = 0;
        while (input >= 1) {
            i = i * 10 + input % 10;
            input /= 10;
        }
        return i;
    }
    static boolean isPalindrom(int number) {
        return number == InvertNumber(number);
    }
}

