package ru.kav.zadachi.zd7;
import java.util.Scanner;
/**
 * Created by user on 25.01.2017.
 */
public class GuessTheNumber {
    public static void main(String[] args) {
        int comp;
        int you;
        comp = (int) (Math.random() * 10);
        do {
            System.out.println("Введите число от 0 до 10");
            Scanner scanner = new Scanner(System.in);
            you = scanner.nextInt();
            if (you == comp) {
                System.out.println("Выйграли");
            } else {
                if (you > comp) {
                    System.out.println("Вы ввели число больше загаданного");
                } else {
                    if (you < comp) {
                        System.out.println("Вы ввели число меньше загаданного");
                    } else {
                        System.out.println("Вы вели неверное число");
                    }
                }
            }

        } while( you != comp );
        System.out.println("Пока");
    }
}
