package ru.kav.zadachi.zd13;
import java.util.Arrays;
/**
 * Created by user on 06.04.2017.
 */
public class TwoMasInOneMas {
        public static void main(String[] args) {
            int[][] randomArray = new int[(int)randomRangeGen(2,10)][(int)randomRangeGen(2,10)];
            for (int i = 0; i < randomArray.length; i++) {
                for (int j = 0; j < randomArray[i].length; j++) {
                    randomArray[i][j] = (int)randomRangeGen(2,10);
                }
            }
            for (int i = 0; i < randomArray.length; i++) {
                System.out.println(Arrays.toString(randomArray[i]));
            }
            System.out.println("\n");
            System.out.println(Arrays.toString(convertTo1D(randomArray)));
        }


        static int[] convertTo1D(int[][] input) {
            int [] targetArr = new int[input.length * input[0].length];
            int tempInt = 0;
            for (int i = 0; i < input.length; i++) {
                for (int j = 0; j < input[i].length; j++) {
                    targetArr[tempInt++] = input[i][j];
                }
            }
            return targetArr;
        }
        static double randomRangeGen(double min, double max) {
            return min + (Math.random()*(max-min+1));
        }
    }


