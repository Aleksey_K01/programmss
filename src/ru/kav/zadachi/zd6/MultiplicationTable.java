package ru.kav.zadachi.zd6;
import java.util.Scanner;
/**
 * Created by admin on 19.01.2017.
 */
public class MultiplicationTable {
    public static void main(String[] args) {
        Scanner scanner = new Scanner (System.in);
        int table;
        System.out.print("Введите число:");
        table=scanner.nextInt();
        for (int b = 1; b <= 10; b++) {
            int result=table  * b;
            System.out.println(table + " * " + b + " = " + result);
        }
    }
}


