package ru.kav.zadachi.zd9;
import java.util.Arrays;
/**
 * Created by user on 06.04.2017.
 */
public class NullMatrix {
    public static void main(String[] args) {
        int[][] matrix = {{4, 1, 5},
                {8, 2, 7},
                {5, 4, 7}};
        for (int i = 0; i < matrix.length; i++) {
            System.out.println(Arrays.toString(matrix[i]));
        }
        System.out.println("-----------------------");
        erase(matrix,0 );
        for (int i = 0; i < matrix.length; i++) {

            System.out.println(Arrays.toString(matrix[i]));
        }
    }

    /**
     * Метод обращающий выбранный столбец в нули.
     * @param matrix
     * @param col
     */
    static void erase(int[][] matrix, int col) {
        for (int i = 0; i < matrix.length; i++) {
            if (col > matrix[i].length) {
            }
            matrix[i][col] = 0;
        }
    }
}
